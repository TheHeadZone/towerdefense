﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace HZ.UI
{
    public class MainMenuCanvas : MonoBehaviour
    {

        //Manage All the subcanvas in the main menu scene
		[SerializeField]Canvas mainMenuButtonsCanvas;

		

		[SerializeField]OptionsCanvas optionsCanvas;

		[SerializeField]Camera mainCamera;

		Canvas currentCanvas;

		

		public void EnterToLevel(){
			SceneManager.LoadScene("HZ_MainLevel");
		}

		public void QuitGame(){
			UnityEditor.EditorApplication.isPlaying = false;
		}
    }

}


