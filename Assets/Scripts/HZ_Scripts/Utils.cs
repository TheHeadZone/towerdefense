﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils {

	public static float Map(float value, float oldRangeMin, float oldRangeMax, float newRangeMin, float newRangeMax){
		float oldRange = oldRangeMax - oldRangeMin;
		float newRange = newRangeMax - newRangeMin;
		return (((value - oldRangeMin) * newRange) / oldRange) + newRangeMin;
	}
}
