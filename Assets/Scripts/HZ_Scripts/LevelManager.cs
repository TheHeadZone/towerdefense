﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HZ
{
    public class LevelManager : MonoBehaviour
    {

        private static LevelManager instance;

        [SerializeField] int playerHealth = 10;
        [SerializeField] int playerResources = 10;

        [SerializeField] LevelUIController uiController;



        //Properties

        public static LevelManager Instance
        {
            get
            {
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public int PlayerResources
        {
            get
            {
                return playerResources;
            }
            set
            {
                playerResources = value;
                uiController.UpdateResources();
            }
        }

        public int PlayerHealth
        {
            get
            {
                return playerHealth;
            }
            set
            {
                playerHealth = value;
				if(playerHealth <= 0){
					uiController.Defeat();
					Time.timeScale = 0;

				}
                uiController.UpdateHealth();
            }
        }

        //Methods

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            StartCoroutine(IncreaseResources());
        }


        IEnumerator IncreaseResources()
        {
            while (PlayerHealth > 0)
            {
                PlayerResources++;
                yield return new WaitForSeconds(3.0f);
            }

        }

    }

}

