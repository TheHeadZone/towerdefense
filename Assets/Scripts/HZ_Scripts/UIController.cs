﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

	[SerializeField] CamaraScript mainCamera;

	[SerializeField]Canvas levelSelection;

	public void LevelSelection(bool isActive){
		if(!levelSelection){
			Debug.Log("Level Selection is not initialized");
		}
		levelSelection.gameObject.SetActive(isActive);
	}
}
