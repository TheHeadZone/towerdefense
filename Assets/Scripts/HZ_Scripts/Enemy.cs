﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


namespace HZ
{
    public class Enemy : MonoBehaviour
    {

		[SerializeField]GameObject explosionPrefab;

		[SerializeField]int health;

        // Use this for initialization
        void Start()
        {
			this.transform.GetComponent<NavMeshAgent>().destination = HZ.EnemyManager.Instance.Target.position;
        }

		void OnTriggerEnter(Collider other){
			if(other.CompareTag("Target")){
				StartCoroutine(Explode());
				this.transform.GetComponent<NavMeshAgent>().speed = 0;
			}
		}


		IEnumerator Explode(){
			yield return new WaitForSeconds(3.0f);
			Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
			HZ.LevelManager.Instance.PlayerHealth -= 3;
			HZ.EnemyManager.Instance.EnemyList.Remove(this.gameObject);
			Destroy(this.gameObject);

		}

		public void ApplyDamage(int damage){
			this.health -= damage;
			if(this.health <= 0){
				Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
				Destroy(this.gameObject);
			}
		}
    }

}