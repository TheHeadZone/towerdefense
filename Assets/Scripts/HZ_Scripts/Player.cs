﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class Player : MonoBehaviour
{

    public GameObject activeTower = null;



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (activeTower != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                activeTower.transform.position = hit.point;
                if (Input.GetMouseButtonDown(0))
                {
                    if (hit.transform.CompareTag("Placement"))
                    {
						activeTower.GetComponent<HZ.Tower>().SetActive = true;
                        activeTower = null;
                        Destroy(hit.collider);
                    }
                }
            }
        }

    }


    public void CreateTower(GameObject towerPrefab)
    {
        if (activeTower == null)
        {
            if (HZ.LevelManager.Instance.PlayerResources >= 4)
            {
                activeTower = Instantiate(towerPrefab, Input.mousePosition, Quaternion.identity);
                HZ.LevelManager.Instance.PlayerResources -= 4;
            }

        }

    }

}
