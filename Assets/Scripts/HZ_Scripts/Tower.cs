﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HZ
{

    public class Tower : MonoBehaviour
    {
        [SerializeField] float range;
        [SerializeField] int damage;
        [SerializeField] GameObject fireFX;
        [SerializeField] float fireSpeed;
        [SerializeField] Transform canonTransform;
        Enemy enemy;

		[SerializeField] Transform TurretHead;

        bool isActive = false;

        GameObject target;


        public bool SetActive
        {
            set
            {
                isActive = value;
            }
        }

        void Update()
        {
            if (target != null)
            {
                TurretHead.LookAt(target.transform, this.transform.up);
            }

        }

        void OnDrawGizmosSelected()
        {
            // Display the explosion radius when selected
            Gizmos.color = new Color(1, 1, 0, 0.75F);
            Gizmos.DrawSphere(transform.position, range);
        }

        private void Start()
        {
            StartCoroutine(SearchEnemy());
            StartCoroutine(Fire());
        }

        IEnumerator SearchEnemy()
        {
            while (true)
            {
                float minDist = Mathf.Infinity;
                int indexOfMin = 0;
                foreach (GameObject enemy in HZ.EnemyManager.Instance.EnemyList)
                {
                    if (enemy == null)
                        continue;
                    float dist = Vector3.Distance(this.transform.position, enemy.transform.position);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        target = enemy;
                    }
                }
                yield return new WaitForSeconds(1.5f);
            }

        }

        IEnumerator Fire()
        {
            while (true)
            {
                if (target != null)
                {
                    if (Vector3.Distance(this.transform.position, target.transform.position) < range)
                    {
                        Instantiate(fireFX, this.canonTransform.position + this.canonTransform.forward * 0.5f, Quaternion.identity);
                        target.GetComponent<Enemy>().ApplyDamage(damage);
                    }

                }
                yield return new WaitForSeconds(fireSpeed);
            }


        }



    }

}

