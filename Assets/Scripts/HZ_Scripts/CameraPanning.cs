﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanning : MonoBehaviour {

	[SerializeField] private float panningSpeed;
	Vector3 mousePosition;

	void Update () {
		mousePosition = new 
		Vector3(Utils.Map(Input.mousePosition.x, 0, Screen.width, -1, 1), 0,
		 Utils.Map(Input.mousePosition.y, 0, Screen.height, -1, 1));


		mousePosition.x = Mathf.Abs(mousePosition.x) >= 0.75f ? mousePosition.x : 0;
		mousePosition.z = Mathf.Abs(mousePosition.z) >= 0.75f ? mousePosition.z : 0;


		//this.transform.Translate(mousePosition * Time.deltaTime * panningSpeed, Space.World);
		this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x + mousePosition.x * Time.deltaTime * panningSpeed, -8.0f, -1.0f), 
		this.transform.position.y, 
		Mathf.Clamp(this.transform.position.z + mousePosition.z * Time.deltaTime * panningSpeed, -25.0f, -10.0f));
	}
}
