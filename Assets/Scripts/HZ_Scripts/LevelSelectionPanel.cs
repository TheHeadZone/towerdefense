﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionPanel : MonoBehaviour {

	[SerializeField]GameObject levelsButtonsPivot;

	[SerializeField] float speed;
	
	private Vector3 mousePosition;
	// Update is called once per frame
	void Update () {
		mousePosition = new 
		Vector3(Utils.Map(Input.mousePosition.x, 0, Screen.width, -1, 1), 0,
		 0);
		mousePosition.x = Mathf.Abs(mousePosition.x) >= 0.75f ? mousePosition.x : 0;
		levelsButtonsPivot.transform.Translate(mousePosition * Time.deltaTime * speed);
	}


}
