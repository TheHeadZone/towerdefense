﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using HZ.UI;

public class CamaraScript : MonoBehaviour {

	[SerializeField]UIController controller;
	bool isOnLevelSelection = false;


	public void ChangeState(){
		this.GetComponent<Animator>().SetBool("LevelSelection", isOnLevelSelection = !isOnLevelSelection);
	}


	public void OpenLevelSelection(){
		
		controller.LevelSelection(true);
	}

	public void CloseLevelSelection(){
		this.GetComponent<Animator>().SetBool("LevelSelection", false);
		controller.LevelSelection(false);
	}
}
