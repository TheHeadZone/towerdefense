﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HZ
{

    public class EnemyManager : MonoBehaviour
    {
        //Additional variable delaration

        public enum EnemyTypes
        {
            Buggy,
            Copter,
            Tank
        };

        [System.Serializable]
        public struct EnemyWave
        {
            public EnemyTypes[] wave;
        }

        //Member variables


        private static EnemyManager instance;

		public Transform spawnPoint;
        public Transform target;


        [SerializeField] EnemyWave[] waves;
		[SerializeField] float inBetweenWaveDelay;
		[SerializeField] float inBetweenEnemiesDelay;
		[SerializeField] GameObject buggyPrefab;
		[SerializeField] GameObject copterPrefab;
		[SerializeField] GameObject tankPrefab;

		private List<GameObject> enemyList = new List<GameObject>();



        //Properties


        public Transform Target
        {
            get { return target; }
        }


        public static EnemyManager Instance
        {
            get
            {
                return instance;
            }
            private set
            {
                instance = value;
            }

        }

        public List<GameObject> EnemyList
        {
            get
            {
                return enemyList;
            }
        }

        //Methods


        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
        }

		public void StartWaveProcess(){
			StartCoroutine(SendWaves());
		}

		public IEnumerator SendWaves(){
			for(int i = 0; i < waves.Length; ++i){
				StartCoroutine(SpawnWave(i));
				yield return new WaitForSeconds(inBetweenWaveDelay);
			}
		}

		IEnumerator SpawnWave(int waveIndex){
			for(int i = 0; i < waves[waveIndex].wave.Length; ++i){
				switch (waves[waveIndex].wave[i])
				{
					
					case EnemyTypes.Buggy:
						EnemyList.Add(Instantiate(buggyPrefab, spawnPoint.position, Quaternion.identity));
						break;
				}
				yield return new WaitForSeconds(inBetweenEnemiesDelay);

			}
		}

    }
}

