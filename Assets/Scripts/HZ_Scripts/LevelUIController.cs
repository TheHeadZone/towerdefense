﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelUIController : MonoBehaviour
{

    [SerializeField] private Text resourcesText;
    [SerializeField] private Text healthText;

    [SerializeField] GameObject pauseButton;
    [SerializeField] GameObject resumeButton;

	[SerializeField] GameObject defeatMenu;

    void Start()
    {
        resourcesText.text = HZ.LevelManager.Instance.PlayerResources.ToString();
        healthText.text = HZ.LevelManager.Instance.PlayerHealth.ToString();
    }

    public void UpdateHealth()
    {
        healthText.text = HZ.LevelManager.Instance.PlayerHealth.ToString();
    }

    public void UpdateResources()
    {
        resourcesText.text = HZ.LevelManager.Instance.PlayerResources.ToString();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        pauseButton.SetActive(false);
        resumeButton.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        resumeButton.SetActive(false);
        pauseButton.SetActive(true);
    }

	public void Defeat(){
		defeatMenu.SetActive(true);
	}

	public void Restart(){
		SceneManager.LoadScene("HZ_MainLevel");
		Time.timeScale = 1;
	}
}
